(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.logo2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgrAoQgQgOAAgbQAAg1BAAAQA3AAAAA8IAAAGIhFAAQABAQAaABQAVgBANgGIAAAbQgSAGgdAAQgfAAgRgPgAgHgWQgDAGABAHIAZAAQAAgUgNgBQgHAAgDAIg");
	this.shape.setTransform(113.2,10.5,0.67,0.67);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgvAwQgNgIAAgQQAAglA3AAIATABQgBgQgZAAQgRAAgSAJIAAgdQAXgGAVAAQAbAAAPAHQAVALABAaIAAA+IgwAAIAAgPIgBAAQgKASgWAAQgRAAgKgHgAgNASQAAAKAMAAQAGAAAFgFQAEgGAAgIIAAgDIgFAAQgWAAAAAMg");
	this.shape_1.setTransform(94.9,10.5,0.67,0.67);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgrA2IAAhoIAuAAIAAAXIABAAQALgaAVAAIAIABIAAAqQgDgBgIAAQgLAAgIAIQgHAHAAAMIAAAmg");
	this.shape_2.setTransform(87.6,10.4,0.67,0.67);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgbAhIAAgkIgSAAIAAgeIASAAIAAgYIAxgQIAAAoIAYAAIAAAeIgYAAIAAAdQAAAOAMAAQAGAAAGgCIAAAeQgSAGgRAAQgmAAAAgpg");
	this.shape_3.setTransform(80.6,9.3,0.67,0.67);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgYBMIAAhoIAxAAIAABogAgYgoIAAgjIAxAAIAAAjg");
	this.shape_4.setTransform(74.9,9,0.67,0.67);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgrA2IAAhoIAvAAIAAAXIAAAAQALgaAVAAIAIABIAAAqQgDgBgIAAQgLAAgIAIQgHAHgBAMIAAAmg");
	this.shape_5.setTransform(69.3,10.4,0.67,0.67);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgrAoQgQgOAAgbQAAg1BAAAQA3AAAAA8IAAAHIhFAAQAAAPAbAAQAVABANgHIAAAbQgSAGgdAAQgfAAgRgPgAgHgWQgDAFAAAIIAaAAQAAgVgNAAQgHAAgDAIg");
	this.shape_6.setTransform(61.3,10.5,0.67,0.67);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AArA2IAAg8QAAgOgJAAQgJAAAAAOIAAA8IgxAAIAAg8QAAgOgJAAQgJAAAAAOIAAA8IgzAAIAAhoIAwAAIAAASIAAAAQAJgVAbAAQALAAAIAFQAKAEADAJQADgIALgFQAKgFAKAAQAZAAAIAPQAEAIAAAWIAAA+g");
	this.shape_7.setTransform(50.2,10.4,0.67,0.67);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAfBGIgKgeIgsAAIgJAeIg2AAIAxiMIBKAAIAyCMgAgQAGIAeAAIgOgtIAAAAg");
	this.shape_8.setTransform(37.4,9.3,0.67,0.67);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("Ag1A9QgKgQAAgZQAAgWAKgOQAMgQAXgBQATABAMAQIABAAIAAg9IAyAAIAACYIgvAAIAAgTIAAAAQgKAWgYgBQgXAAgNgQgAgKAUQAAAXAMAAQANAAAAgWQgBgVgNAAQgLAAAAAUg");
	this.shape_9.setTransform(104,9,0.67,0.67);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AiABzIAAjmIEBAAIAADmgAgTBCIA7AAQAfAAANgMQAQgQABgpQgBgigNgQQgMgPgbAAIiVAAIAAATIAsAAIAABzIAdAAIAAhzIBCAAQAQAAAHAKQAHALAAAaQAAAZgHALQgGAMgQAAIgcAAIAAhYIgeAAg");
	this.shape_10.setTransform(12.9,11.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#50B948").s().p("Ah+BxIAAjiID8AAIAADig");
	this.shape_11.setTransform(13,11.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_11).wait(1));

}).prototype = getMCSymbolPrototype(lib.logo2, new cjs.Rectangle(0,0,117.2,23.1), null);


(lib.greenrectlight = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#43B02A").s().p("EgMfAu4MAAAhdvIY/AAMAAABdvg");
	this.shape.setTransform(80,300);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.greenrectlight, new cjs.Rectangle(0,0,160,600), null);


(lib.click = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#044F04").s().p("A3bTiMAAAgnDMAu3AAAMAAAAnDg");
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.c2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgKAKQgEgDAAgHQAAgFAEgEQAFgFAFAAQAGAAAFAFQAEAEAAAFQAAAHgEADQgFAEgGABQgFgBgFgEg");
	this.shape.setTransform(229.8,-13.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAsBGQgDAAgDgCQgCgCgCgDIgKgaIgvAAIgKAaQgBADgDACQgDACgDAAIgCAAIgCgBQgFgCgBgEQgCgEABgEIAsh1QACgDADgCQADgCACAAQAEAAACACQADACACADIAsB1QABAEgBAEQgCAEgFACIgBABIgDAAgAAQAPIgQgqIgPAqIAfAAg");
	this.shape_1.setTransform(220.6,-18.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgsBDQgDgDAAgFIAAh1QAAgFADgDQADgDAFAAIAkAAQALAAAJAGQAKAFAGALQAFAJAAAMQAAAKgEAIQgEAIgHAGIAEADIADAEIAFAKQABAGAAAGIABAHIAAAEIAEADIADAEQACAFgCAEQgCAEgFACIgCAAIgCAAIgEAAIgEgCIgEgDIgDgEIgDgKIgBgOIgBgFIgBgEIgDgDIgDgDIgHgDIgDgBIgYAAIAAArQAAAFgDADQgEADgEAAQgFAAgDgDgAgZgGIAZAAQAHAAAGgFQAGgHAAgIQAAgJgGgGQgGgFgHgBIgZAAg");
	this.shape_2.setTransform(207.9,-18.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgHBDQgDgDAAgFIAAh1QAAgFADgDQADgDAEAAQAFAAADADQADADAAAFIAAB1QAAAFgDADQgDADgFAAQgEAAgDgDg");
	this.shape_3.setTransform(198.5,-18.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAlBEIgDgDIgBAAIgBgBIg+hYIAABSQgBAFgCADQgDADgFAAQgFAAgDgDQgDgDAAgFIAAhzIAAAAIABgFQABgDACgBQAFgDADABQAFAAACAEIBABYIAAhRQAAgEAEgEQADgDAEAAQAEAAADADQAEAEAAAEIAABzQAAAFgEADQgDADgEAAIgFgBg");
	this.shape_4.setTransform(181.2,-18.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAsBGQgDAAgDgCQgCgCgCgDIgKgaIgvAAIgKAaQgBADgDACQgDACgDAAIgCAAIgCgBQgFgCgBgEQgCgEABgEIAsh1QACgDADgCQADgCACAAQAEAAACACQADACACADIAsB1QABAEgBAEQgCAEgFACIgBABIgDAAgAAQAPIgQgqIgPAqIAfAAg");
	this.shape_5.setTransform(167.7,-18.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAmBEIgEgDIgBAAIgBgBIg+hYIAABSQAAAFgDADQgDADgFAAQgFAAgDgDQgDgDAAgFIAAhzIAAAAIABgFQABgDACgBQAFgDADABQAFAAADAEIA/BYIAAhRQAAgEAEgEQADgDAEAAQAEAAADADQAEAEAAAEIAABzQAAAFgEADQgDADgEAAIgEgBg");
	this.shape_6.setTransform(146.9,-18.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgfBGIgBAAIAAAAQgEAAgDgDQgEgDAAgFIAAh1QAAgFAEgDQADgDAEAAIAAAAIABAAIBAAAQAEAAAEADQADADAAAFQAAAEgDAEQgEADgEAAIg1AAIAAAlIAtAAQAFAAADADQADADAAAEQAAAFgDACQgDAEgFAAIgtAAIAAAlIA1AAQAEAAAEADQADAEAAAEQAAAFgDADQgEADgEAAg");
	this.shape_7.setTransform(134.2,-18.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgpBDQgDgDAAgFIAAh1QAAgFADgDQADgDAFAAIAAAAIAjAAQAMAAAKAGQAJAFAGALQAGAJAAAMQAAAMgGAKQgGAIgJAFQgKAHgMAAIgYAAIAAArQAAAFgDADQgDADgFAAQgFAAgDgDgAgWgGIAYAAQAJAAAFgFQAGgHABgIQgBgJgGgGQgFgFgJgBIgYAAg");
	this.shape_8.setTransform(122.6,-18.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgaBCQgMgGgJgJQgKgKgFgNQgGgNABgPQgBgOAGgNQAFgNAKgKQAJgJAMgGQANgGANAAQAOAAANAGQAMAGAKAJQAIAKAGANQAFANABAOQgBAPgFANQgGANgIAKQgKAJgMAGQgNAGgOAAQgNAAgNgGgAgRgsQgHADgHAHQgHAGgDAKQgDAJgBAJQABAKADAJQADAKAHAGQAHAHAHADQAJAEAIAAQAKAAAHgEQAJgDAGgHQAHgGADgKQAEgJgBgKQABgJgEgJQgDgKgHgGQgGgHgJgDQgHgEgKAAQgIAAgJAEg");
	this.shape_9.setTransform(107.4,-18.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.c2, new cjs.Rectangle(97,-32.7,136.4,27.3), null);


(lib.c1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgHAIQgEgDAAgFQAAgEAEgDQADgEAEAAQAFAAADAEQAEADAAAEQAAAFgEADQgDAEgFAAQgEAAgDgEg");
	this.shape.setTransform(254.3,5.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgTAyQgOgIgHgNQgIgNgBgQQABgPAIgNQAHgNAOgIQANgHAPgBQALAAAJADQAJAEAIAGQADACABAEQAAAEgCADQgDADgDAAQgEABgDgDQgFgEgHgCQgGgCgIgBQgIAAgHADQgHAEgFAFQgGAGgDAHQgDAHAAAHQAAAIADAHQADAHAGAGQAFAFAHAEQAHADAIAAQAGAAAGgCQAGgCAEgDIAAgVIgWAAQgEgBgDgDQgCgCAAgEQAAgDACgCQADgDAEAAIAfAAQAEAAADADQADACAAADIAAAkIgBABIAAABIAAABIAAAAIAAAAIgBACIgCACQgIAGgJADQgJADgLABQgPAAgNgIg");
	this.shape_1.setTransform(244.9,0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAeA3IgDgCIAAgBIgBAAIgyhGIAABBQAAAEgDACQgCADgEAAQgDAAgDgDQgCgCAAgEIAAhcIAAgEIADgEQADgCAEABQADABACACIAzBHIAAhBQAAgEACgCQADgDADAAQAEAAADADQACACAAAEIAABcQAAAEgCACQgDADgEAAIgDgBg");
	this.shape_2.setTransform(232.3,0.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgFA1QgCgCgBgEIAAhdQABgEACgDQACgCADAAQAEAAADACQACADAAAEIAABdQAAAEgCACQgDADgEAAQgDAAgCgDg");
	this.shape_3.setTransform(224.2,0.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAhA1QgCgCgBgEIAAhCIgXAfIgDADIgEABIAAAAIgDgBIgDgDIgXgfIAABCQAAAEgDACQgDADgDAAQgEAAgDgDQgCgCAAgEIAAhdQAAgEACgDQADgCAEAAIAAAAIAEABIADACIAfAtIAggtIADgCIAEgBQAEAAADACQACADAAAEIAABdQAAAEgCACQgDADgEAAQgEAAgCgDg");
	this.shape_4.setTransform(214.5,0.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgUA1QgKgEgIgIQgHgJgEgKQgEgKgBgMQABgLAEgLQAEgJAHgJQAIgIAKgEQAKgEAKgBQAMABAKAEQAJAEAIAIQAHAJAEAJQAFALAAALQAAAMgFAKQgEAKgHAJQgIAIgJAEQgKAEgMABQgKgBgKgEgAgNgkQgGADgFAGQgGAFgCAIQgDAGAAAIQAAAIADAHQACAIAGAFQAFAFAGAEQAHACAGABQAIgBAGgCQAHgEAFgFQAFgFADgIQADgHAAgIQAAgIgDgGQgDgIgFgFQgFgGgHgDQgGgDgIAAQgGAAgHADg");
	this.shape_5.setTransform(201,0.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgTAyQgNgIgIgNQgIgNgBgQQABgPAIgNQAIgNANgIQANgIAPAAQALAAAJADQAKAEAHAGQADACAAAEQABAEgCADQgDADgEAAQgDAAgDgCQgFgEgHgCQgGgCgIgBQgIAAgHADQgHADgGAGQgFAFgDAIQgDAGAAAIQAAAIADAHQADAIAFAFQAGAFAHADQAHADAIABQAIgBAGgCQAHgCAFgEQADgCADAAQAEAAADADQACADgBAEQAAAEgDACQgHAGgKADQgJADgLABQgPAAgNgIg");
	this.shape_6.setTransform(188.2,0.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgPA3IgOgGIgLgIQgDgCAAgDQAAgDACgDQACgDAEAAQADAAADACIAIAFQAFAEAGABQAFACAFAAQAGAAAFgBIAJgFQAEgCABgDQACgDAAgEIAAgCIgBgCIgCgDIgDgDIgJgDIgMgEIgLgCIgKgEIgJgEIgGgIIgDgGIgBgHQAAgHAEgGQADgGAGgEQAGgFAHgCQAHgCAHAAQAHAAAHACQAGACAFAEIAKAFQACACABADQABAEgCADQgCADgDAAQgDABgDgCQgGgFgHgDQgGgCgHAAQgFgBgEACIgIAEIgFAFIgBAFIAAADIABABIACACIACADIAJAEIAKADIABAAIALADIALADIAJAGQAEADADAEIACAGIACAHQgBAJgDAGQgEAGgGAFQgGADgIADQgHACgJABQgIgBgHgCg");
	this.shape_7.setTransform(172.1,0.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgFA1QgDgCAAgEIAAhdQAAgEADgDQACgCADAAQADAAADACQADADAAAEIAABdQAAAEgDACQgDADgDAAQgDAAgCgDg");
	this.shape_8.setTransform(164.4,0.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgFA1QgDgDAAgDIAAgsIgggsQgCgDAAgDQAAgDADgCQADgCAEAAQAEABABACIAbAlIAcglQABgCAEgBQAEAAADACQADACAAADQAAADgCADIghAsIAAAsQAAADgCADQgDACgDABQgCgBgDgCg");
	this.shape_9.setTransform(152.6,0.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAkA4QgDAAgCgCQgDgBgBgCIgHgWIgmAAIgIAWQgBACgDABQAAABgBAAQAAAAgBABQAAAAgBAAQgBAAAAAAIgCAAIgCgBQgDgBgCgDQgBgDABgEIAkhdQABgDACgCIAEgBQADAAACABQACACABADIAkBdQABAEgBADQgCADgDABIgCABIgBAAgAANAMIgNghIgMAhIAZAAg");
	this.shape_10.setTransform(142.3,0.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AggA4QgEAAgCgDQgDgCAAgEIAAhdQAAgEADgDQACgCAEAAIATAAQAPAAAMAIQANAHAHAMQAIAOAAAOQAAAQgIAMQgHANgNAIQgMAHgPAAgAgXAmIAKAAQALAAAHgFQAJgFAFgJQAFgJAAgKQAAgKgFgJQgFgIgJgFQgHgFgLAAIgKAAg");
	this.shape_11.setTransform(131.2,0.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAeA2IgDgCIgbglIgaAlIgDACIgEABIgDAAIgCgCQgDgCgBgDQAAgEACgDIAegpIgegpQgCgDAAgDQABgEADgCQADgCADABQAEAAACADIAaAlIAbglQACgDAEAAQADgBADACQADACABAEQAAADgCADIgeApIAeApQACADAAAEQgBADgDACIgCACIgDAAIgEgBg");
	this.shape_12.setTransform(114.6,0.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAkA4QgDAAgCgCQgDgBgBgCIgHgWIgmAAIgIAWQgBACgDABQAAABgBAAQAAAAgBABQAAAAgBAAQAAAAgBAAIgCAAIgCgBQgDgBgCgDQgBgDABgEIAkhdQABgDACgCIAEgBQADAAACABQACACABADIAkBdQABAEgBADQgCADgDABIgCABIgBAAgAANAMIgNghIgMAhIAZAAg");
	this.shape_13.setTransform(104.1,0.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgGA1QgCgCAAgEIAAhUIgZAAQgEgBgDgCQgCgDAAgDQAAgEACgDQADgCAEAAIBDAAQAEAAACACQADADAAAEQAAADgDADQgCACgEABIgZAAIAABUQgBAEgCACQgCADgEAAQgDAAgDgDg");
	this.shape_14.setTransform(93.5,0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.c1, new cjs.Rectangle(77.4,-11,189.2,22.7), null);


// stage content:
(lib.TDA_Urgency_Tackle_300x25 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var self = this;
		//this.btnColor;
		////this.downText = this.cta.cta.textDown;
		////this.upText = this.cta.cta.textUp;
		////this.textRestingYPos = 17;
		//this.ctaTextOffset = 30;
		
		////self.upText.y = this.textRestingYPos + this.ctaTextOffset;
		////self.downText.y = this.textRestingYPos;
		
		//this.setBtnColor = function(p_color) {
		//	switch (p_color) {
		//		case "white":
		//			self.cta.cta.arrow.gotoAndPlay("dark-green");
		//			//self.cta.cta.textDown.gotoAndPlay("dark-green");
		//			self.cta.cta.bg.gotoAndPlay("white");
		//			self.btnColor = "white";
		//			break;
		//		case "regular-green":
		//			self.cta.cta.arrow.gotoAndPlay("white");
		//			//self.cta.cta.textDown.gotoAndPlay("white");
		//			self.cta.cta.bg.gotoAndPlay("regular-green");
		//			self.btnColor = "regular-green";
		//			break;	
		//	}
		//}
		//this.setRolloverBtnColor = function() {
		//	if (self.btnColor == "regular-green") {
		//		setTimeout(function() {
		//			self.cta.cta.arrow.gotoAndPlay("dark-green");
		//		}, 150);
		//	}	
		//}
		//this.setRolloutBtnColor = function() {
		//	if (self.btnColor == "regular-green") {
		//		setTimeout(function() {
		//			self.cta.cta.arrow.gotoAndPlay("white");
		//		}, 150);
		//	}
		//}
		
		//// Set initial button color
		//setTimeout(function() {
		//	self.setBtnColor("regular-green");
		//}, 50);
		
		// Clicktag =======================
		
		this.clicktag.addEventListener('click', handleClick);
		//this.clicktag.addEventListener('mouseover', handleMouseOver);
		//this.clicktag.addEventListener('mouseout', handleMouseOut);
		
		function handleClick() {
		    window.ctaClick();
		}
		//function handleMouseOver() {
		//	//var landingY = self.textRestingYPos;
		//	self.cta.gotoAndPlay("over"); 
		//	self.cta.cta.gotoAndPlay("over");
		//	self.setRolloverBtnColor();
		//	//self.downText.y = landingY;
		//	//self.upText.y = landingY-self.ctaTextOffset;
		//	//createjs.Tween.get(self.downText, {override:true}).to({y:landingY+self.ctaTextOffset}, 200, createjs.Ease.quintIn);
		//	//createjs.Tween.get(self.upText, {override:true}).wait(200).to({y:landingY}, 300, createjs.Ease.quintOut);
		//}
		//function handleMouseOut() {
		//	//var landingY = self.textRestingYPos;
		//	self.cta.gotoAndPlay("out");
		//	self.cta.cta.gotoAndPlay("out");
		//	self.setRolloutBtnColor();
		//	//self.downText.y = landingY+self.ctaTextOffset;
		//	//self.upText.y = landingY;
		//	//createjs.Tween.get(self.downText, {override:true}).wait(200).to({y:landingY}, 300, createjs.Ease.quintOut);
		//	//createjs.Tween.get(self.upText, {override:true}).to({y:landingY-self.ctaTextOffset}, 200, createjs.Ease.quintIn);
		//}
		//
	}
	this.frame_61 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(61).call(this.frame_61).wait(1));

	// stroke
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,0,3).p("A2uiQMAtdAAAIAAEhMgtdAAAg");
	this.shape.setTransform(150.1,12.5,1.027,0.828);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(62));

	// ct
	this.clicktag = new lib.click();
	this.clicktag.name = "clicktag";
	this.clicktag.parent = this;
	this.clicktag.setTransform(150.4,12.6,1,0.1,0,0,0,0.5,1.5);
	new cjs.ButtonHelper(this.clicktag, 0, 1, 2, false, new lib.click(), 3);

	this.timeline.addTween(cjs.Tween.get(this.clicktag).wait(62));

	// logo
	this.instance = new lib.logo2();
	this.instance.parent = this;
	this.instance.setTransform(45.8,13.2,0.665,0.665,0,0,0,59.1,12);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(62));

	// c1
	this.instance_1 = new lib.c2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(59.9,18,1,1,0,0,0,37.2,16);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(51).to({_off:false},0).to({y:48},8).wait(3));

	// c1
	this.instance_2 = new lib.c1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(58.7,-1,1,1,0,0,0,37.2,16);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({y:29},9).wait(38).to({y:64},8).to({_off:true},1).wait(6));

	// green bg
	this.instance_3 = new lib.greenrectlight();
	this.instance_3.parent = this;
	this.instance_3.setTransform(150.4,12.6,1.874,0.042,0,0,0,80.2,301.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(62));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(149.6,-15.5,301,53.5);
// library properties:
lib.properties = {
	id: 'D756A44A46C14633B6ABBD4A07193522',
	width: 300,
	height: 25,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D756A44A46C14633B6ABBD4A07193522'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;